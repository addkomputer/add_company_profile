<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>CV. ADD KOMPUTER</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="<?= base_url(); ?>assets/img/logo.png" rel="icon">
    <link href="<?= base_url(); ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Scaffold - v2.0.0
  * Template URL: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex">

            <div class="logo mr-auto">
                <div class="text-inline">
                    <!-- <img src="<?= base_url(); ?>assets/img/logo.png" alt="logo" class="mr-1"> -->
                    <!-- Uncomment below if you prefer to use an image logo -->
                    <a href="index.html"><img src="<?= base_url(); ?>assets/img/logo.png" alt="" class="img-fluid mr-1"></a>
                    <!-- <h1 class="text-light"><a href="index.html"><span>ADD Komputer</span></a></h1> -->
                </div>
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li><a href="#hero">Home</a></li>
                    <li><a href="#lowongan">Lowongan</a></li>
                    <li><a href="#cta">Tentang Kami</a></li>
                    <li><a href="#client">Clients</a></li>
                    <li><a href="#contact">Kontak</a></li>

                </ul>
            </nav><!-- .nav-menu -->

            <div class="header-social-links">
                <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
                <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
                <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
                <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
            </div>

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">

        <div class="container">
            <div class="row">
                <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="fade-up">
                    <div>
                        <h1>Membuat langkah semakin mudah</h1>
                        <h2>Tim kami akan membantu anda untuk mempermudah pekerjaan anda dengan aplikasi yang bisa di akses dari manapun</h2>
                        <a href="#lowongan" class="btn-get-started scrollto">Lowongan Pekerjaan</a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
                    <img src="<?= base_url(); ?>assets/img/hero-img.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="lowongan" class="about">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6" data-aos="zoom-in">
                        <img src="<?= base_url(); ?>assets/img/network_engineer.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 d-flex flex-column justify-contents-center" data-aos="fade-left">
                        <div class="content pt-4 pt-lg-0">
                            <h3>Lowongan Teknisi Komputer & Jaringan</h3>
                            <p class="font-italic">
                                Kami sedang membutuhkan tenagga kerja baru sebagai Teknisi Komputer & Jaringan, berikut beberapa keriteria yang kami inginkan :
                            </p>
                            <ul>
                                <li><i class="icofont-check-circled"></i> Laki-Laki.</li>
                                <li><i class="icofont-check-circled"></i> Lulusan SMK (Jurusan Elektronika, Teknik Komputer Jaringan)</li>
                                <li><i class="icofont-check-circled"></i> Attitude baik, tekun belajar, kemampuan bekerja dalam tim.</li>
                                <li><i class="icofont-check-circled"></i> Usia Max 24 th.</li>
                                <li><i class="icofont-check-circled"></i> Penempatan Jogja & Kebumen.</li>
                            </ul>
                            <p>
                                Dengan ini kami ucapkan banyak terimakasih kepada semua pihak yang telah berpartisipas.
                            </p>
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLScsCAkv9K3Y_KC8nyoGseCIRNMljb8BlwS9HwaES7PYT2EnEQ/viewform" target="_blank">
                                <button class="btn btn-primary btn-get-started">Daftar Sekarang</button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End About Section -->

        <section class="about">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6" data-aos="zoom-in">
                        <img src="<?= base_url(); ?>assets/img/about.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 d-flex flex-column justify-contents-center" data-aos="fade-left">
                        <div class="content pt-4 pt-lg-0">
                            <h3>Lowongan Programmer</h3>
                            <p class="font-italic">
                                Kami sedang membutuhkan tenagga kerja baru sebagai Programer, berikut beberapa keriteria yang kami inginkan :
                            </p>
                            <ul>
                                <li><i class="icofont-check-circled"></i> Laki-Laki.</li>
                                <li><i class="icofont-check-circled"></i> Pendidikan SMK Komputer (TKJ, RPL, Multimedia)</li>
                                <li><i class="icofont-check-circled"></i> Attitude baik, tekun belajar, kemampuan bekerja dalam tim.</li>
                                <li><i class="icofont-check-circled"></i> Memiliki kemampuan logika pemrograman yang baik.</li>
                                <li><i class="icofont-check-circled"></i> Memiliki kemampuan bahasa pemrograman PHP native (framework nilai tambah).</li>
                                <li><i class="icofont-check-circled"></i> Memiliki kemampuan desain (nilai tambah).</li>
                            </ul>
                            <p>
                                Dengan ini kami ucapkan banyak terimakasih kepada semua pihak yang telah berpartisipas.
                            </p>
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLScsCAkv9K3Y_KC8nyoGseCIRNMljb8BlwS9HwaES7PYT2EnEQ/viewform" target="_blank">
                                <button class="btn btn-primary btn-get-started">Daftar Sekarang</button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End About Section -->

        <!-- ======= Features Section ======= -->
        <section id="features" class="features">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6 mt-2 mb-tg-0 order-2 order-lg-1">
                        <ul class="nav nav-tabs flex-column">
                            <li class="nav-item" data-aos="fade-up">
                                <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                                    <h4>Pendaftaran</h4>
                                    <p>Calon pekerja wajib melakukan pendaftaran sebelum ke tahap berikutnya.</p>
                                </a>
                            </li>
                            <li class="nav-item mt-2" data-aos="fade-up" data-aos-delay="100">
                                <a class="nav-link" data-toggle="tab" href="#tab-2">
                                    <h4>Wawancara</h4>
                                    <p>Setelah mendaftar, calon pekerja akan di wawancara untuk menentukan keputusan.</p>
                                </a>
                            </li>
                            <li class="nav-item mt-2" data-aos="fade-up" data-aos-delay="300">
                                <a class="nav-link" data-toggle="tab" href="#tab-4">
                                    <h4>Pengumuman</h4>
                                    <p>Setelah keputusan di ambil, calon pekerja yang di terima akan di hubungi oleh kami melalui kontak yang di daftarkan.</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 order-1 order-lg-2" data-aos="zoom-in">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="tab-1">
                                <figure>
                                    <img src="<?= base_url(); ?>assets/img/features-2.png" alt="" class="img-fluid">
                                </figure>
                            </div>
                            <div class="tab-pane" id="tab-2">
                                <figure>
                                    <img src="<?= base_url(); ?>assets/img/features-1.png" alt="" class="img-fluid">
                                </figure>
                            </div>
                            <div class="tab-pane" id="tab-4">
                                <figure>
                                    <img src="<?= base_url(); ?>assets/img/features-4.png" alt="" class="img-fluid">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Features Section -->


        <!-- ======= Cta Section ======= -->
        <section id="cta" class="cta">
            <div class="container">

                <div class="row" data-aos="zoom-in">
                    <div class="col-lg-9 text-center text-lg-left">
                        <h3>Tentang Kami</h3>
                        <p> Kami merupakan sebuah perusahaan yang bergerak di bidang teknologi, produk yang kami hasilkan adalah software aplikasi berbasis web maupun android. Kebanyakan client kami berasal dari dinas pemerintah, selain itu kami juga memiliki client RSUD maupun rumah sakit swasta.</p>
                    </div>
                    <div class="col-lg-3 cta-btn-container text-center">
                        <a class="cta-btn align-middle" href="#contact">Hubungi Kami</a>
                    </div>
                </div>

            </div>
        </section><!-- End Cta Section -->

        <!-- ======= services Section ======= -->
        <section id="client" class="services">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>Clients</h2>
                </div>

                <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/jawa-tengah.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Prov. Jawa Tengah</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/kebumen.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Kebumen</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in" data-aos-delay="100">
                            <img src="<?= base_url(); ?>assets/img/clients/semarang.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Semarang</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in" data-aos-delay="150">
                            <img src="<?= base_url(); ?>assets/img/clients/klaten.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Klaten</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in" data-aos-delay="200">
                            <img src="<?= base_url(); ?>assets/img/clients/banjarnegara.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Banjarnegara</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/tegal.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kota. Tegal</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/wonogiri.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Wonogiri</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/boyolali.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Boyolali</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/sleman.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Sleman</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/madiun.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Madiun</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/banyumas.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Banyumas</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6 col-sm-6 col-6">
                        <div class="client-logo text-center" data-aos="zoom-in">
                            <img src="<?= base_url(); ?>assets/img/clients/temanggung.png" class="img-fluid my-2" style="width: 8em; height: 10.5em;" alt="">
                            <h5>Kab. Temanggung</h5>
                        </div>
                    </div>

                </div>

            </div>
        </section><!-- End Clients Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact section-bg">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>Kontak Kami</h2>
                </div>

                <div class="row">

                    <div class="col-lg-5 d-flex align-items-stretch" data-aos="fade-right">
                        <div class="info">
                            <div class="address">
                                <i class="icofont-google-map"></i>
                                <h4>Lokasi:</h4>
                                <p>Jl. Menayu Lor, Menayu Lor, Tirtonirmolo, Kec. Kasihan, Bantul, Daerah Istimewa Yogyakarta 55184</p>
                            </div>

                            <div class="email">
                                <i class="icofont-envelope"></i>
                                <h4>Email:</h4>
                                <p>info@addkomputer.com</p>
                            </div>

                            <div class="phone">
                                <i class="icofont-phone"></i>
                                <h4>Telepon:</h4>
                                <p>+62 878-3829-5450 (Malik)</p>
                            </div>

                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15810.657466150287!2d110.3373672!3d-7.8253034!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7e0a9bfde0ebf33d!2sCV.%20ADD%20KOMPUTER!5e0!3m2!1sen!2sid!4v1601539748503!5m2!1sen!2sid" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
                        </div>

                    </div>

                    <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-left">
                        <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Your Name</label>
                                    <input type="text" name="name" class="form-control" id="name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validate"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name">Your Email</label>
                                    <input type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validate"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Subject</label>
                                <input type="text" class="form-control" name="subject" id="subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                <div class="validate"></div>
                            </div>
                            <div class="form-group">
                                <label for="name">Message</label>
                                <textarea class="form-control" name="message" rows="10" data-rule="required" data-msg="Please write something for us"></textarea>
                                <div class="validate"></div>
                            </div>
                            <div class="mb-3">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>
                            </div>
                            <div class="text-center"><button type="submit">Send Message</button></div>
                        </form>
                    </div>

                </div>

            </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

    <!-- modal -->
    <div class="modal fade bd-example-modal-lg" id="form-pendaftaran" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="form">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="exampleModalLongTitle">Pendaftaran</h5>
                </div>
                <div class="modal-body">
                    <form id="main-form" autocomplete="off">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Nama:</label>
                                    <input type="text" class="form-control form-control-sm" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="tgl_lahir" class="col-form-label">Tanggal Lahir:</label>
                                    <input type="date" class="form-control datepicker form-control-sm" id="tgl_lahir" name="tgl_lahir" value="dd-mm-yyyy">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control form-control-sm" id="email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="no_hp" class="col-form-label">Tlp/Whatsapp:</label>
                                    <input type="number" class="form-control form-control-sm" id="no_hp" name="no_hp">
                                </div>
                                <div class="form-group">
                                    <label for="alamat" class="col-form-label">Alamat:</label>
                                    <textarea class="form-control form-control-sm" id="alamat" name="alamat"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-12"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" form="main-form" class="btn btn-primary">Kirim Lamaran</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- ======= Footer ======= -->
    <footer id="footer">

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>ADD Komputer</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/ -->
                Designed by <a href="addkomputer.com">ADD team</a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top"><i class="bx bxs-up-arrow-alt"></i></a>

    <!-- Vendor JS Files -->
    <script src="<?= base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/php-email-form/validate.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/venobox/venobox.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/aos/aos.js"></script>

    <!-- Template Main JS File -->
    <script src="<?= base_url(); ?>assets/js/main.js"></script>
    <script>
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });
    </script>

</body>

</html>